package io.cafeliang.menuorder.data.user;


import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.UUID;

public class User {
    @NonNull
    private final String id;

    @Nullable
    private String name;

    public User(@Nullable String displayName) {
        this(UUID.randomUUID().toString(), displayName);
    }

    public User(@NonNull String id, @Nullable String displayName) {
        this.id = id;
        this.name = displayName;
    }

    @NonNull
    public String getId() {
        return id;
    }

    @Nullable
    public String getName() {
        return name;
    }
}
