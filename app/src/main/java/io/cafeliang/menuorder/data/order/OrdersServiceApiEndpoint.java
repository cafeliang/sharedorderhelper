package io.cafeliang.menuorder.data.order;

import android.support.v4.util.ArrayMap;

import java.util.Arrays;

import io.cafeliang.menuorder.data.user.LocalUser;
import io.cafeliang.menuorder.data.user.User;

/**
 * Simulate a real server end point
 */
public final class OrdersServiceApiEndpoint {

    private final static ArrayMap<String, Order> DATA;

    static {
        DATA = new ArrayMap(3);
        addOrders();
    }

    private static void addOrders() {
        Order order1 = new Order("Ohana Hawaiian BBQ");
        order1.setOrderItems(
                Arrays.asList(
                        new OrderItem("Hawaiian BBQ Chicken",
                                      LocalUser.getUser()),
                        new OrderItem("Chicken Katsu",
                                      new User("Zhou"),
                                      new User("Arbit")),
                        new OrderItem("Grilled Chicken Breast"),
                        new OrderItem("Island Fire Chicken")));
        DATA.put(order1.getId(), order1);

        Order order2 = new Order("It's boba time");
        order2.setOrderItems(
                Arrays.asList(
                        new OrderItem("Citron Honey Tea",
                                      new User("Zhou"),
                                      new User("Dipak")),
                        new OrderItem("Green Tea Latte",
                                      LocalUser.getUser()),
                        new OrderItem("Milk Foam Oolong Tea")));
        DATA.put(order2.getId(), order2);

        Order order3 = new Order("Tasty Kitchen");
        order3.setOrderItems(
                Arrays.asList(
                        new OrderItem("Kungpao chicken",
                                      new User("Arbit Chen")),
                        new OrderItem("House special fried rice")));
        DATA.put(order3.getId(), order3);
    }

    public static ArrayMap<String, Order> loadOrders(){
        return DATA;
    }
}
