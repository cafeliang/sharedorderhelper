package io.cafeliang.menuorder.data.order;

import android.support.annotation.NonNull;

public class OrderRepositories {

    private static OrdersRepository repository = null;

    public synchronized static OrdersRepository getInMemoryRepoInstance(@NonNull OrdersServiceApi serviceApi){
        if (repository == null){
            repository = new InMemoryOrdersRepository(serviceApi);
        }

        return repository;
    }
}
