package io.cafeliang.menuorder.data.user;


import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

public class LocalUser {
    private static User user;

    public static void setUser(GoogleSignInAccount account) {
        String displayName =
                (account.getDisplayName() == null
                        || account.getDisplayName().isEmpty())
                        ? "You" : account.getDisplayName();
        LocalUser.user = new User(account.getId(), displayName);
    }

    public static void setUser(User user) {
        LocalUser.user = user;
    }

    public static User getUser(){
        return user;
    }
}
