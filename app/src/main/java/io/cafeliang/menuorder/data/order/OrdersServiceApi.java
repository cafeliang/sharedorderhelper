package io.cafeliang.menuorder.data.order;


import java.util.List;

import io.cafeliang.menuorder.data.user.User;

public interface OrdersServiceApi {

    interface OrdersServiceCallback<T> {

        void onLoaded(T orders);
    }

    void getAllOrders(OrdersServiceCallback<List<Order>> callback);

    void getOrder(String orderId, OrdersServiceCallback<Order> callback);

    void saveOrder(Order order);

    void addUser(String orderId, String orderItemId, User user);

    void removeUser(String orderId, String orderItemId, User user);
}

