package io.cafeliang.menuorder.data.order;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class Order {
    private static final String TAG = Order.class.getSimpleName();

    @NonNull
    private final String id;

    @Nullable
    private final String title;

    private Map<String, OrderItem> orderItems;

    public Order(@Nullable String title) {
        this.id = UUID.randomUUID().toString();
        this.title = title;
        orderItems = Collections.emptyMap();
    }

    @NonNull
    public String getId() {
        return id;
    }

    @Nullable
    public String getTitle() {
        return title;
    }

    public void setOrderItems(List<OrderItem> orderItems) {
        this.orderItems = new HashMap<>();

        for (OrderItem item: orderItems) {
            this.orderItems.put(item.getId(), item);
        }
    }

    @NonNull
    public List<OrderItem> getOrderItems(){
        List<OrderItem> items = new ArrayList<>(orderItems.values());
        Collections.sort(items, orderItemComparator);
        return items;
    }
    private Comparator<OrderItem> orderItemComparator = new Comparator<OrderItem>() {
        @Override
        public int compare(OrderItem item1, OrderItem item2) {
            return item2.getCount() - item1.getCount();
        }
    };

    public boolean hasOrderItem(String orderItemId) {
        return orderItems.containsKey(orderItemId);
    }

    public boolean hasOrderItem(OrderItem item) {
        return orderItems.containsKey(item.getId());
    }

    @Nullable
    public OrderItem getOrderItem(String orderItemId) {
        if (hasOrderItem(orderItemId)){
            return orderItems.get(orderItemId);
        } else {
            Log.w(TAG, "getOrderItem: order " + id + "doesn't contain order item " + orderItemId);
            return null;
        }
    }

    public boolean isEmpty(){
        return title == null || title.isEmpty();
    }
}
