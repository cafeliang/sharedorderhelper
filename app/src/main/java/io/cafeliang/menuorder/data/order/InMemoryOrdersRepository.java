package io.cafeliang.menuorder.data.order;

import android.support.annotation.NonNull;

import java.util.List;

import io.cafeliang.menuorder.data.user.User;

import static com.google.common.base.Preconditions.checkNotNull;

class InMemoryOrdersRepository implements OrdersRepository {
    private static final String TAG = InMemoryOrdersRepository.class.getSimpleName();
    private final OrdersServiceApi ordersServiceApi;

    List<Order> cachedOrders;

    public InMemoryOrdersRepository(@NonNull OrdersServiceApi serviceApi) {
        ordersServiceApi = checkNotNull(serviceApi);
    }

    @Override
    public void getOrders(@NonNull final LoadOrdersCallback callback) {
        if (cachedOrders == null) {
            ordersServiceApi.getAllOrders(new OrdersServiceApi.OrdersServiceCallback<List<Order>>() {
                @Override
                public void onLoaded(List<Order> orders) {
                    cachedOrders = orders;
                    callback.onOrdersLoaded(cachedOrders);
                }
            });
        } else {
            callback.onOrdersLoaded(cachedOrders);
        }
    }

    @Override
    public void getOrder(@NonNull final String orderId, @NonNull final GetOrderCallback callback) {
        ordersServiceApi.getOrder(orderId, new OrdersServiceApi.OrdersServiceCallback<Order>() {
            @Override
            public void onLoaded(Order orders) {
                callback.onOrderLoaded(orders);
            }
        });
    }

    @Override
    public void saveOrder(@NonNull Order order) {
        ordersServiceApi.saveOrder(order);
        refreshData();
    }

    @Override
    public void addUser(@NonNull String orderId, @NonNull String orderItemId, User user) {
        ordersServiceApi.addUser(orderId, orderItemId, user);
        refreshData();
    }

    @Override
    public void removeUser(@NonNull String orderId, @NonNull String orderItemId, User user) {
        ordersServiceApi.removeUser(orderId, orderItemId, user);
    }

    @Override
    public void refreshData() {
        cachedOrders = null;
    }
}
