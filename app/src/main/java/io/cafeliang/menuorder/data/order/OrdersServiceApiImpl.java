package io.cafeliang.menuorder.data.order;

import android.support.annotation.NonNull;
import android.support.v4.util.ArrayMap;

import java.util.ArrayList;
import java.util.List;

import io.cafeliang.menuorder.data.user.User;

public class OrdersServiceApiImpl implements OrdersServiceApi {

    private static final ArrayMap<String, Order> ORDERS =
            OrdersServiceApiEndpoint.loadOrders();

    @Override
    public void getAllOrders(OrdersServiceCallback<List<Order>> callback) {
        List<Order> orders = new ArrayList<>(ORDERS.values());
        callback.onLoaded(orders);
    }

    @Override
    public void getOrder(String orderId, OrdersServiceCallback<Order> callback) {
        Order order = ORDERS.get(orderId);
        callback.onLoaded(order);
    }

    @Override
    public void saveOrder(Order order) {
        ORDERS.put(order.getId(), order);
    }

    @Override
    public void addUser(String orderId, String orderItemId, @NonNull User user) {
        Order order = getOrder(orderId);
        OrderItem orderItem = order.getOrderItem(orderItemId);

        if (orderItem != null) {
            orderItem.addUser(user);
        }
    }

    @Override
    public void removeUser(String orderId, String orderItemId, User user) {
        Order order = getOrder(orderId);
        OrderItem orderItem = order.getOrderItem(orderItemId);
        if (orderItem != null) {
            orderItem.removeUser(user);
        }
    }

    @NonNull
    private Order getOrder(String orderId) {
        if (ORDERS.containsKey(orderId)) {
            return ORDERS.get(orderId);
        } else {
            throw new IllegalArgumentException("Fail to add user: order id does not exist.");
        }
    }
}
