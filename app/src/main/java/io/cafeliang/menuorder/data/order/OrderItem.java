package io.cafeliang.menuorder.data.order;


import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import io.cafeliang.menuorder.data.user.User;

/**
 * Item inside {@link Order}
 */
public class OrderItem {

    @NonNull
    private final String id;

    @NonNull
    private final String title;

    /**
     * key: user id
     * value: user display name
     */
    private Map<String, String> users = Collections.emptyMap();

    public OrderItem(@NonNull String title, User... users) {
        this.id = UUID.randomUUID().toString();
        this.title = title;

        this.users = new HashMap<>();
        for (User user: users) {
            this.users.put(user.getId(), user.getName());
        }
    }

    @NonNull
    public String getId(){
        return id;
    }

    @NonNull
    public String getTitle() {
        return title;
    }

    public int getCount() {
        return users.size();
    }

    public boolean containsUser(@NonNull User user){
        return users.containsKey(user.getId());
    }

    public List<String> getUserNames(){
        return new ArrayList<>(users.values());
    }

    public void addUser(@NonNull User user){
        users.put(user.getId(), user.getName());
    }

    public void removeUser(@NonNull User user) {
        users.remove(user.getId());
    }

}
