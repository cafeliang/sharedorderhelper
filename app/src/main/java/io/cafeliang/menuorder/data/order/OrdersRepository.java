package io.cafeliang.menuorder.data.order;

import android.support.annotation.NonNull;

import java.util.List;

import io.cafeliang.menuorder.data.user.User;


public interface OrdersRepository {


    interface LoadOrdersCallback {
        void onOrdersLoaded(List<Order> orders);
    }

    interface GetOrderCallback {
        void onOrderLoaded(Order order);
    }

    void getOrders(@NonNull LoadOrdersCallback callback);

    void getOrder(@NonNull String orderId, @NonNull GetOrderCallback callback);

    void saveOrder(@NonNull Order order);

    void addUser(@NonNull String orderId, @NonNull String orderItemId, User user);

    void removeUser(@NonNull String orderId, @NonNull String orderItemId, User user);

    void refreshData();
}
