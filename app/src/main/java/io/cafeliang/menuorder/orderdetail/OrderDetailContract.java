package io.cafeliang.menuorder.orderdetail;


import java.util.List;

import io.cafeliang.menuorder.data.order.OrderItem;
import io.cafeliang.menuorder.data.user.User;

interface OrderDetailContract {
    interface View {
        void showTitle(String title);

        void showOrderItems(List<OrderItem> orderItems, User localUser);

        void showEmptyOrder();
    }

    interface UserActionListener {
        void openOrder(String orderId);

        void selectOrderItem(String orderId, String orderItemId, boolean select);
    }
}
