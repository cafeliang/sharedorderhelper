package io.cafeliang.menuorder.orderdetail;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.List;

import io.cafeliang.menuorder.R;
import io.cafeliang.menuorder.data.order.OrderItem;
import io.cafeliang.menuorder.data.user.User;

class OrderItemsAdapter extends RecyclerView.Adapter<OrderItemsAdapter.ViewHolder>{
    private List<OrderItem> orderItems;
    private User localUser;

    private OrderItemListener orderItemListener;

    public OrderItemsAdapter(List<OrderItem> orderItems, OrderItemListener itemListener) {
        this.orderItems = orderItems;
        this.orderItemListener = itemListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_orderitem, parent, false);

        return new ViewHolder(view, orderItemListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        OrderItem item = orderItems.get(position);
        holder.checkbox.setChecked(item.containsUser(localUser));
        holder.title.setText(item.getTitle());
        holder.count.setText(String.valueOf(item.getCount()));

        holder.users.setText(makeUsersText(item.getUserNames()));
    }

    private String makeUsersText(List<String> userNames) {
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < userNames.size(); i++) {
            if (i > 0) stringBuilder.append("\n");
            stringBuilder.append(userNames.get(i));
        }
        return stringBuilder.toString();
    }

    @Override
    public int getItemCount() {
        return orderItems.size();
    }

    public void updateData(List<OrderItem> orderItems, User localUser){
        this.orderItems = orderItems;
        this.localUser = localUser;

        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public CheckBox checkbox;
        public TextView title;
        public TextView count;
        public TextView users;

        private OrderItemListener orderItemListener;
        public ViewHolder(View itemView, OrderItemListener listener){
            super(itemView);
            this.orderItemListener = listener;

            checkbox = itemView.findViewById(R.id.orderitem_checkbox);
            title = itemView.findViewById(R.id.orderitem_title);
            count = itemView.findViewById(R.id.orderitem_count);
            users = itemView.findViewById(R.id.orderitem_users);

            itemView.setOnClickListener(this);
            checkbox.setClickable(false);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            OrderItem orderItem = orderItems.get(position);
            boolean targetSelectState = !checkbox.isChecked();
            orderItemListener.onClick(orderItem, targetSelectState);
            notifyDataSetChanged();
        }
    }
}
