package io.cafeliang.menuorder.orderdetail;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import io.cafeliang.menuorder.Injection;
import io.cafeliang.menuorder.R;
import io.cafeliang.menuorder.data.order.OrderItem;
import io.cafeliang.menuorder.data.user.LocalUser;
import io.cafeliang.menuorder.data.user.User;

public class OrderDetailFragment extends Fragment implements OrderDetailContract.View {
    public static final String ARG_ORDER_ID = "order_id";

    private OrderDetailContract.UserActionListener actionListener;
    private OrderItemsAdapter itemsAdapter;

    private TextView titleTextView;

    public OrderDetailFragment() {
        // Required constructor
    }

    public static OrderDetailFragment newInstance(String orderId) {
        Bundle args = new Bundle();
        args.putString(ARG_ORDER_ID, orderId);
        OrderDetailFragment fragment = new OrderDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        itemsAdapter = new OrderItemsAdapter(Collections.<OrderItem>emptyList(), itemListener);
    }

    OrderItemListener itemListener = new OrderItemListener() {
        @Override
        public void onClick(OrderItem orderItem, boolean select) {
            String orderId = getArguments().getString(ARG_ORDER_ID);
            actionListener.selectOrderItem(orderId, orderItem.getId(), select);
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_detail, container, false);

        titleTextView = root.findViewById(R.id.order_detail_title);
        RecyclerView recyclerView = root.findViewById(R.id.order_items);
        recyclerView.setAdapter(itemsAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        actionListener = new OrderDetailPresenter(Injection.provideOrdersRepository(), LocalUser.getUser(), this);
    }

    @Override
    public void onResume() {
        super.onResume();
        String orderId = getArguments().getString(ARG_ORDER_ID);
        actionListener.openOrder(orderId);
    }

    @Override
    public void showTitle(String title) {
        titleTextView.setText(title);
    }

    @Override
    public void showOrderItems(List<OrderItem> orderItems, User localUser) {
        itemsAdapter.updateData(orderItems, localUser);
    }


    @Override
    public void showEmptyOrder() {
        titleTextView.setText("");
    }
}
