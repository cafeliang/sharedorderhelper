package io.cafeliang.menuorder.orderdetail;

import android.support.annotation.NonNull;

import io.cafeliang.menuorder.data.order.Order;
import io.cafeliang.menuorder.data.order.OrdersRepository;
import io.cafeliang.menuorder.data.user.User;

class OrderDetailPresenter implements OrderDetailContract.UserActionListener {

    private final OrdersRepository ordersRepository;
    private User localUser;

    OrderDetailContract.View orderDetailView;

    public OrderDetailPresenter(@NonNull OrdersRepository ordersRepository, @NonNull User localUser, @NonNull OrderDetailContract.View orderDetailView) {
        this.ordersRepository = ordersRepository;
        this.localUser = localUser;
        this.orderDetailView = orderDetailView;
    }

    @Override
    public void openOrder(String orderId) {
        if (orderId == null || orderId.isEmpty()) {
            return;
        }

        ordersRepository.getOrder(orderId, new OrdersRepository.GetOrderCallback() {
            @Override
            public void onOrderLoaded(Order order) {
                if (order == null) {
                    orderDetailView.showEmptyOrder();
                } else {
                    orderDetailView.showTitle(order.getTitle());
                    orderDetailView.showOrderItems(order.getOrderItems(), localUser);
                }
            }
        });
    }

    @Override
    public void selectOrderItem(String orderId, String orderItemId, boolean select) {
        if (select)
            ordersRepository.addUser(orderId, orderItemId, localUser);
        else
            ordersRepository.removeUser(orderId, orderItemId, localUser);
    }
}
