package io.cafeliang.menuorder.orderdetail;

import io.cafeliang.menuorder.data.order.OrderItem;

public interface OrderItemListener {
    void onClick(OrderItem orderItem, boolean select);
}
