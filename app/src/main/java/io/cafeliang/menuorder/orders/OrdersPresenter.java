package io.cafeliang.menuorder.orders;

import android.support.annotation.NonNull;

import java.util.List;

import io.cafeliang.menuorder.data.order.Order;
import io.cafeliang.menuorder.data.order.OrdersRepository;

class OrdersPresenter implements OrdersContract.UserActionListener {

    private final OrdersRepository ordersRepository;
    private final OrdersContract.View ordersView;

    public OrdersPresenter(@NonNull OrdersRepository ordersRepository, @NonNull OrdersContract.View ordersView) {
        this.ordersRepository = ordersRepository;
        this.ordersView = ordersView;
    }

    @Override
    public void loadOrders(boolean forceUpdate) {
        if (forceUpdate) {
            ordersRepository.refreshData();
        }

        ordersRepository.getOrders(new OrdersRepository.LoadOrdersCallback() {
            @Override
            public void onOrdersLoaded(List<Order> orders) {
                ordersView.showOrders(orders);
            }
        });

    }

    @Override
    public void addOrder() {
        ordersView.showAddOrder();
    }

    @Override
    public void openOrderDetails(@NonNull Order order) {
        ordersView.showOrderDetailUi(order.getId());
    }
}
