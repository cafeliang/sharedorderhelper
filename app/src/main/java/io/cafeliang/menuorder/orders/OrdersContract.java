package io.cafeliang.menuorder.orders;

import android.support.annotation.NonNull;

import java.util.List;

import io.cafeliang.menuorder.data.order.Order;

public class OrdersContract {

    interface View {
        void showOrders(List<Order> orders);

        void showAddOrder();

        void showOrderDetailUi(String orderId);
    }

    interface UserActionListener {
        void loadOrders(boolean forceUpdate);

        void addOrder();

        void openOrderDetails(@NonNull Order order);
    }
}
