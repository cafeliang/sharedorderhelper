package io.cafeliang.menuorder.orders;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import io.cafeliang.menuorder.Injection;
import io.cafeliang.menuorder.R;
import io.cafeliang.menuorder.data.order.Order;
import io.cafeliang.menuorder.orderdetail.OrderDetailActivity;

import static com.google.common.base.Preconditions.checkNotNull;


public class OrdersFragment extends Fragment implements OrdersContract.View {
    private OrdersContract.UserActionListener actionListener;
    private OrdersAdapter ordersAdapter;

    public OrdersFragment() {
        // Required constructor
    }

    public static OrdersFragment newInstance() {
        return new OrdersFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ordersAdapter = new OrdersAdapter(Collections.<Order>emptyList(), orderListener);
        actionListener = new OrdersPresenter(Injection.provideOrdersRepository(), this);
    }

    OrderListener orderListener = new OrderListener() {
        @Override
        public void onClick(Order order) {
            actionListener.openOrderDetails(order);
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_orders, container, false);
        RecyclerView recyclerView = root.findViewById(R.id.orders);
        recyclerView.setAdapter(ordersAdapter);

        int columns = getContext().getResources().getInteger(R.integer.num_orders_columns);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), columns));

        FloatingActionButton fab = getActivity().findViewById(R.id.fab_add_orders);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(v, "Add order function is under construction", Snackbar.LENGTH_LONG).show();
            }
        });

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        actionListener.loadOrders(false);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void showOrders(List<Order> orders) {
        ordersAdapter.updateData(orders);
    }

    @Override
    public void showAddOrder() {
        // TODO: 12/8/18  
    }

    @Override
    public void showOrderDetailUi(String orderId) {
        Intent intent = new Intent(getContext(), OrderDetailActivity.class);
        intent.putExtra(OrderDetailActivity.EXTRA_ORDER_ID, orderId);
        startActivity(intent);
    }

    private static class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.ViewHolder> {
        private List<Order> orders;
        private OrderListener orderListener;

        public OrdersAdapter(List<Order> orders, OrderListener orderListener) {
            this.orders = checkNotNull(orders);
            this.orderListener = orderListener;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_order, parent, false);
            return new ViewHolder(view, orderListener);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            Order order = orders.get(position);
            holder.title.setText(order.getTitle());
        }

        @Override
        public int getItemCount() {
            return orders.size();
        }

        public void updateData(List<Order> orders) {
            this.orders = checkNotNull(orders);
            notifyDataSetChanged();
        }

        public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            public TextView title;

            private OrderListener orderListener;
            public ViewHolder(View orderView, OrderListener listener){
                super(orderView);
                this.orderListener = listener;
                title = orderView.findViewById(R.id.order_title);
                orderView.setOnClickListener(this);
            }

            @Override
            public void onClick(View view) {
                int position = getAdapterPosition();
                Order order = orders.get(position);
                orderListener.onClick(order);
            }
        }
    }

    public interface OrderListener {

        void onClick(Order order);
    }
}
