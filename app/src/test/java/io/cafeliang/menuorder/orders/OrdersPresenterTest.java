package io.cafeliang.menuorder.orders;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import io.cafeliang.menuorder.data.order.Order;
import io.cafeliang.menuorder.data.order.OrdersRepository;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;

public class OrdersPresenterTest {

    private static List<Order> ORDERS;

    @Mock
    private OrdersRepository ordersRepository;

    @Mock
    private OrdersContract.View ordersView;

    @Captor
    private ArgumentCaptor<OrdersRepository.LoadOrdersCallback> loadOrdersCallbackCaptor;

    private OrdersPresenter ordersPresenter;

    static {
        Order order = new Order("Ohana Hawaiian BBQ");
        ORDERS = new ArrayList<>();
        ORDERS.add(order);
    }

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        ordersPresenter = new OrdersPresenter(ordersRepository, ordersView);
    }

    @Test
    public void loadOrdersFromRepo_loadIntoView() throws Exception {
        // given
        ordersPresenter.loadOrders(true);

        // when
        verify(ordersRepository).getOrders(loadOrdersCallbackCaptor.capture());
        loadOrdersCallbackCaptor.getValue().onOrdersLoaded(ORDERS);

        // then
        verify(ordersView).showOrders(ORDERS);
    }

    @Test
    public void clickOrder_showDetailUI() throws Exception {
        // given
        Order order = new Order("Ohana Hawaiian BBQ");

        // when
        ordersPresenter.openOrderDetails(order);

        // then
        verify(ordersView).showOrderDetailUi(any(String.class));
    }
}