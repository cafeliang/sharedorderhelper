package io.cafeliang.menuorder.data.order;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.cafeliang.menuorder.data.user.LocalUser;
import io.cafeliang.menuorder.data.user.User;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({LocalUser.class})
public class InMemoryOrdersRepositoryTest {
    private static List<Order> ORDERS;
    private static String ORDER_ID;

    static {
        Order order = new Order("Ohana Hawaiian BBQ");
        ORDER_ID = order.getId();
        order.setOrderItems(
                Arrays.asList(
                        new OrderItem("Hawaiian BBQ Chicken",
                                      new User("Kai-Hui")),
                        new OrderItem("Chicken Katsu",
                                      new User("Zhou"),
                                      new User("Arbit")),
                        new OrderItem("Grilled Chicken Breast"),
                        new OrderItem("Island Fire Chicken")));

        ORDERS = new ArrayList<>();
        ORDERS.add(order);
    }

    private InMemoryOrdersRepository ordersRepository;

    @Mock
    private OrdersServiceApiImpl serviceApi;

    @Mock
    private OrdersRepository.LoadOrdersCallback loadOrdersCallback;

    @Mock
    private OrdersRepository.GetOrderCallback getOrderCallback;

    @Captor
    private ArgumentCaptor<OrdersServiceApi.OrdersServiceCallback> ordersServiceCallbackArgumentCaptor;


    @Before
    public void setupOrdersRepository() {
        MockitoAnnotations.initMocks(this);
        mockStatic(LocalUser.class);
        when(LocalUser.getUser()).thenReturn(new User("Local User"));

        ordersRepository = new InMemoryOrdersRepository(serviceApi);
    }

    @Test
    public void getOrders_repositoryCachesAfterFirstApiCall() {
        twoLoadCallsToRepository(loadOrdersCallback);

        verify(serviceApi).getAllOrders(any(OrdersServiceApi.OrdersServiceCallback.class));
    }

    @Test
    public void invalidateCache_DoesNotCallTheServiceApi() {
        // given
        twoLoadCallsToRepository(loadOrdersCallback);

        // when
        ordersRepository.refreshData();
        ordersRepository.getOrders(loadOrdersCallback); // Third call to API

        // Then
        verify(serviceApi, times(2)).getAllOrders(any(OrdersServiceApi.OrdersServiceCallback.class));
    }


    @Test
    public void getOrders_requestsAllOrdersFromServiceApi() {
        // When
        ordersRepository.getOrders(loadOrdersCallback);

        // Then
        verify(serviceApi).getAllOrders(any(OrdersServiceApi.OrdersServiceCallback.class));
    }

    @Test
    public void getOrder_requestsSingleOrderFromServiceApi() {
        // When
        ordersRepository.getOrder(ORDER_ID, getOrderCallback);

        // Then
        verify(serviceApi).getOrder(eq(ORDER_ID), any(OrdersServiceApi.OrdersServiceCallback.class));
    }

    /**
     * Helper method that issues two calls to the orders repository
     */
    private void twoLoadCallsToRepository(OrdersRepository.LoadOrdersCallback callback) {
        ordersRepository.getOrders(callback); // First call to API

        verify(serviceApi).getAllOrders(ordersServiceCallbackArgumentCaptor.capture());

        ordersServiceCallbackArgumentCaptor.getValue().onLoaded(ORDERS);

        ordersRepository.getOrders(callback); // Second call to API
    }
}