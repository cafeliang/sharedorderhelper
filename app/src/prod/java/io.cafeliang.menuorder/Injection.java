package io.cafeliang.menuorder;


import io.cafeliang.menuorder.data.order.OrderRepositories;
import io.cafeliang.menuorder.data.order.OrdersRepository;
import io.cafeliang.menuorder.data.order.OrdersServiceApiImpl;

public class Injection {
    public static OrdersRepository provideOrdersRepository(){
        return OrderRepositories.getInMemoryRepoInstance(new OrdersServiceApiImpl());
    }
}
